# Uni Burnout

## Table of Contents  
- [Introduction](#introduction) 
- [Control](#control)
- [Features](#features)
- [Updates](#updates)
- [Future](#future)
- [Notes](#notes)

## Introduction

[![](http://i3.ytimg.com/vi/E4up6nv7R5I/hqdefault.jpg)](https://www.youtube.com/watch?v=E4up6nv7R5I&t=240s) [![](http://i3.ytimg.com/vi/5KDiuHrOehk/hqdefault.jpg)](https://www.youtube.com/watch?v=5KDiuHrOehk)

This is an unfinished short game that can be played through. Due to time constraints, I'm making the game available for download as a demo.
It contains some bugs. I plan to remake it in the future with my own improved models, expanded story and better gameplay.
Attention: Don’t play Haunted Room, if you are afraid of horror games or any kind of jumpscares. 
For the younger audience I recommend you stop watching this video immediately. This game is a kind of FPS horror survival adventure and escaperoom, 
I’ve created with **Unity 2019.3.4f1 (64-Bit)**. For this game I’ve spent two months to finished it. 
Actually, Haunted Room was just a project for the course Computer Games of my Master studies of Computer Science at the [Fulda University of Applied Sciences](https://www.hs-fulda.de/en/home), 
but I’ve decided to continue developing this game on my own. Haunted Room is based on my nightmares from my childhood. 
Gameplay: You woke up in an unknown dark house. You don’t know where you are and how you ended up in this room. 
Although the fireplace is lightning you can’t see absolutely nothing in this room by midnight. 
The only way to see is with the flashlight you’ve got on you. But beware: The batteries of your flashlight have its limit. 
Just switch on the light if you need it, otherwise the batteries will die. When you have no options to turn on any light after one minute the Ghost of Darkness
is coming to you and you’d pay with your life. The main goal of the game is to escape from the spooky room. This might be dangerous. 
Solve puzzles in the darkness, try to stay alive and, most important… don’t be fooled by your own fear. If you liked FNAF, 
you’ll like HauntedRoom just as much. Please, subscribe this channel, if you want to see more of Haunted Room or maybe other projects of the indie game 
developer Ramorama Interactive.

The demo of the game can be downloaded [**here**](https://ramoramainteractive.itch.io/haunted-room-demo).

## Control
* Control of the game is explained in the game
* U to take out / put in the flashlight
* F to switch on / off the flashlight
* W, A, D, S for moving forward, left, right and backwards
* Space to jump
* Right mouse click to pick up or use items
* E for interacting with Objects
* Rotating middle mouse button for changing the weapons
* Left mouse click to attack
* Tab to switch the inventory item
* F1 to open the menu

## Features
* Health counter
* Energie bar for the Flashlight
* Mix of first-person-shooter, survival game, adventure game and horror game
* Jumpscares

## Updates
No Updates yet.

## Future
A completely new extended full version is planned with deep storyline, custom 3D graphic models and more puzzles.

## Notes
* Make sure, you open the game with [**Unity 2019.3.4f1 (64-bit)**](https://unity3d.com/de/get-unity/download/archive).
* Most of the Models are from the [**Unity Assetstore**](https://assetstore.unity.com/) and [**Sketchfab**](https://sketchfab.com).
* Sounds are from [**Freesound.org**](https://freesound.org/)
* This project has been stored via [**Git LFS**](https://git-lfs.github.com/).
